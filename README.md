# API to convert JSON object to CSV using json2csv

One of the most used utility while developing applications, is to _download JSON data in CSV_.
For example, converting array of objects fetched from database, and converting to CSV as report format.

A simple application which accepts a `GET` request, uses `json2csv` library to generate CSV from JSON object.

## Install express.js

```javascript
npm install express --save
```
Simple program to create and run server using express.
```javascript
const express = require('express');
const app = express();

app.listen(3000, function() {
  console.log('Running on 3000!');
});
```

## Install json2csv by npm

```javascript
npm install json2csv --save
```

For more info on *_json2csv_*, visit [here](https://www.npmjs.com/package/json2csv).

Include it in the application.
```javascript
const json2csv = require('json2csv');
```

## Defining sample JSON array of objects

Consider that similar kind of data is fetched after queries on database and as a result, you have a JSON array of objects.
```javascript
let jsonObj = [
  {
    name : 'Amit',
    age  : 23
  },
  {
    name : 'Ayub',
    age  : 24
  },
  {
    name : 'Afaque',
    age  : 24
  },
  {
    name : 'Aniket',
    age  : 23
  }
];
```

## Building the `GET` request API

```javascript
const CONVERT = '/json/to/csv';

app.get(CONVERT, (req, res) => {
  // declaring the fields of json object
  let fields = ['name', 'age'];
  // declaring the field names of the CSV header
  let fieldNames = ['Name', 'Age']; 
  // converting json to csv => params :: data, fields, fieldNames
  let csv = json2csv({ data: jsonObj, fields: fields, fieldNames: fieldNames });
  // writing the parsed json to output.csv
  fs.writeFile(__dirname + '/output.csv', csv, (err, data) => {
    if (err) {
      res.status(404).send(err);
    } else {
      let file = __dirname + '/output.csv';
      let fileName = 'output.csv';
      res.download(file, fileName, function(err) {
        if (err) {
          res.status(400).send(err);
        } else {
          res.status(200);
        }
      });
    }
  });
});
```