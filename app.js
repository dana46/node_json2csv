const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const fs = require('fs');
const json2csv = require('json2csv');

app.use(bodyParser.urlencoded({extended : true}));

app.use(bodyParser.json());

const CONVERT = '/json/to/csv';

// defining a simple json array
let jsonObj = [
  {
    name : 'Amit',
    age  : 23
  },
  {
    name : 'Ayub',
    age  : 24
  },
  {
    name : 'Afaque',
    age  : 24
  },
  {
    name : 'Aniket',
    age  : 23
  }
];

app.get(CONVERT, (req, res) => {
  // declaring the fields of json object
  let fields = ['name', 'age'];
  // declaring the field names of the CSV header
  let fieldNames = ['Name', 'Age']; 
  // converting json to csv => params :: data, fields, fieldNames
  let csv = json2csv({ data: jsonObj, fields: fields, fieldNames: fieldNames });
  // writing the parsed json to output.csv
  fs.writeFile(__dirname + '/output.csv', csv, (err, data) => {
    if (err) {
      res.status(404).send(err);
    } else {
      let file = __dirname + '/output.csv';
      let fileName = 'output.csv';
      res.download(file, fileName, function(err) {
        if (err) {
          res.status(400).send(err);
        } else {
          res.status(200);
        }
      });
    }
  });
});

app.listen(3000, function() {
  console.log('Running in 3000!');
})